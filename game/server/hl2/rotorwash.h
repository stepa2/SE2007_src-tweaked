//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef ROTORWASH_H
#define ROTORWASH_H
#ifdef _WIN32
#pragma once
#endif

#include "te_particlesystem.h"

class CTERotorWash : public CTEParticleSystem
{
public:
	DECLARE_CLASS( CTERotorWash, CTEParticleSystem );
	DECLARE_SERVERCLASS();

					CTERotorWash( const char *name = "noname");
	virtual 		~CTERotorWash();

	virtual void	Test( const Vector& current_origin, const QAngle& current_angles ) { };
	virtual	void	Create( IRecipientFilter& filter, float delay = 0.0f );

	CNetworkVector( m_vecDown );
	CNetworkVar( float, m_flMaxAltitude );
};

CBaseEntity *CreateRotorWashEmitter( const Vector &localOrigin, const QAngle &localAngles, CBaseEntity *pOwner, float flAltitude );

#endif // ROTORWASH_H
