//
// New shaders collection
//
//  These shaders are compiled as shader model 2.0/2.0b
//  using the new compiler.
//	    _ps20.vcs
//	    _ps20b.vcs
//	    _vs20.vcs
//

aftershock_ps2x.fxc
aftershock_vs20.fxc

Bloom_ps2x.fxc
screenspaceeffect_vs20.fxc

bloomadd_ps2x.fxc

cable_ps2x.fxc
cable_vs20.fxc

cloak_blended_pass_ps2x.fxc
cloak_blended_pass_vs20.fxc

cloak_ps2x.fxc
cloak_vs20.fxc

core_ps2x.fxc
core_vs20.fxc

emissive_scroll_blended_pass_dx8_ps11.psh
emissive_scroll_blended_pass_dx8_vs11.vsh
emissive_scroll_blended_pass_ps2x.fxc
emissive_scroll_blended_pass_vs20.fxc

eye_refract_ps2x.fxc
eye_refract_vs20.fxc

Eyes.psh
Eyes.vsh
eyes_ps2x.fxc
eyes_vs20.fxc

eyes_flashlight_ps11.fxc
eyes_flashlight_vs11.vsh
eyes_flashlight_ps2x.fxc
eyes_flashlight_vs20.fxc

eyeglint_vs20.fxc
eyeglint_ps2x.fxc

flashlight_ps11.fxc
flashlight_ps2x.fxc

flesh_interior_blended_pass_ps2x.fxc
flesh_interior_blended_pass_vs20.fxc

LightmappedGeneric.psh
LightmappedGeneric_AddBaseAlphaMaskedEnvMap.psh
LightmappedGeneric_AddEnvMapMaskNoTexture.psh
LightmappedGeneric_AddEnvMapNoTexture.psh
LightmappedGeneric_BaseAlphaMaskedEnvMapV2.psh
LightmappedGeneric_BaseTexture.psh
LightmappedGeneric_BaseTexture.vsh
lightmappedgeneric_basetextureblend.psh
lightmappedgeneric_basetextureblend.vsh
LightmappedGeneric_BumpmappedEnvmap.psh
lightmappedgeneric_bumpmappedenvmap.vsh
LightmappedGeneric_BumpmappedEnvmap_ps14.psh
lightmappedgeneric_bumpmappedenvmap_ps14.vsh
LightmappedGeneric_BumpmappedLightmap.psh
LightmappedGeneric_SSBumpmappedLightmap.psh
lightmappedgeneric_bumpmappedlightmap.vsh
LightmappedGeneric_BumpmappedLightmap_Base_ps14.psh
LightmappedGeneric_BumpmappedLightmap_Base_ps14.vsh
LightmappedGeneric_BumpmappedLightmap_Blend_ps14.psh
LightmappedGeneric_BumpmappedLightmap_Blend_ps14.vsh
LightmappedGeneric_Decal.psh
LightmappedGeneric_Decal.vsh
LightmappedGeneric_decal_ps2x.fxc
LightmappedGeneric_decal_vs20.fxc
LightmappedGeneric_Detail.psh
LightmappedGeneric_DetailNoTexture.psh
LightmappedGeneric_DetailSelfIlluminated.psh
LightmappedGeneric_EnvMapNoTexture.psh
LightmappedGeneric_EnvMapV2.psh
lightmappedgeneric_flashlight_vs11.fxc
lightmappedgeneric_flashlight_vs20.fxc
LightmappedGeneric_LightingOnly.vsh
LightmappedGeneric_LightingOnly_Overbright2.psh
lightmappedgeneric_lightingonly_overbright2_ps11.fxc
lightmappedgeneric_lightingonly_vs11.fxc
LightmappedGeneric_MaskedEnvMapNoTexture.psh
LightmappedGeneric_MaskedEnvMapV2.psh
LightmappedGeneric_MultiplyByLighting.psh
LightmappedGeneric_MultiplyByLightingNoTexture.psh
LightmappedGeneric_MultiplyByLightingSelfIllum.psh
LightmappedGeneric_NoTexture.psh
lightmappedgeneric_ps2x.fxc
LightmappedGeneric_SelfIlluminated.psh
LightmappedGeneric_SelfIlluminatedEnvMapV2.psh
LightmappedGeneric_SelfIlluminatedMaskedEnvMapV2.psh
lightmappedgeneric_vs11.vsh
lightmappedgeneric_vs20.fxc

lightmappedreflective_ps2x.fxc
lightmappedreflective_vs20.fxc

Refract_ps2x.fxc
Refract_vs20.fxc

skin_ps20b.fxc
skin_vs20.fxc

splinecard_vsxx.fxc

spritecard_ps2x.fxc
spritecard_vsxx.fxc
spritecard_ps11.fxc

teeth_ps2x.fxc
teeth_vs20.fxc
teeth_bump_ps2x.fxc
teeth_bump_vs20.fxc
teeth_flashlight_ps2x.fxc
teeth_flashlight_vs20.fxc


unlitgeneric.psh
UnlitGeneric_BaseTimesDetail.psh
UnlitGeneric_BaseAlphaMaskedEnvMap.psh
UnlitGeneric_Detail.psh
UnlitGeneric_DetailBaseAlphaMaskedEnvMap.psh
UnlitGeneric_DetailEnvMap.psh
UnlitGeneric_DetailEnvMapMask.psh
UnlitGeneric_DetailEnvMapMaskNoTexture.psh
UnlitGeneric_DetailEnvMapNoTexture.psh
UnlitGeneric_DetailNoTexture.psh
UnlitGeneric_EnvMap.psh
UnlitGeneric_EnvMapMask.psh
UnlitGeneric_EnvMapMaskNoTexture.psh
UnlitGeneric_EnvMapNoTexture.psh
UnlitGeneric_LightingOnly.vsh
UnlitGeneric_NoTexture.psh
unlitgeneric_notexture_ps11.fxc
unlitgeneric_notexture_ps2x.fxc
unlitgeneric_ps2x.fxc
UnlitGeneric_MaskBaseByDetailAlpha_ps11.fxc
UnlitGeneric_vs11.vsh
unlitgeneric_vs20.fxc

UnlitTwoTexture.vsh
UnlitTwoTexture.psh
unlittwotexture_ps2x.fxc
unlittwotexture_vs20.fxc

vertexlit_and_unlit_generic_bump_ps2x.fxc
vertexlit_and_unlit_generic_bump_vs20.fxc

vertexlit_and_unlit_generic_ps2x.fxc
vertexlit_and_unlit_generic_vs20.fxc

VertexLitGeneric.psh
VertexLitGeneric_BaseAlphaMaskedEnvMapV2.psh
VertexLitGeneric_Detail.psh
VertexLitGeneric_Detail_LerpBase.psh
VertexLitGeneric_Detail_additive.psh
VertexLitGeneric_Detail_additive_selfillum.psh
VertexLitGeneric_DetailBaseAlphaMaskedEnvMapV2.psh
VertexLitGeneric_DetailEnvMapV2.psh
VertexLitGeneric_DetailMaskedEnvMapV2.psh
VertexLitGeneric_DetailNoTexture.psh
VertexLitGeneric_DetailSelfIlluminated.psh
VertexLitGeneric_DetailSelfIlluminatedEnvMapV2.psh
VertexLitGeneric_DetailSelfIlluminatedMaskedEnvMapV2.psh
VertexLitGeneric_EnvMapNoTexture.psh
VertexLitGeneric_EnvmappedBumpmap_NoLighting.vsh
VertexLitGeneric_EnvmappedBumpmap_NoLighting_ps14.vsh
VertexLitGeneric_EnvmappedBumpmapV2.psh
VertexLitGeneric_EnvmappedBumpmapV2_MultByAlpha.psh
VertexLitGeneric_EnvmappedBumpmapV2_MultByAlpha_ps14.psh
VertexLitGeneric_EnvmappedBumpmapV2_ps14.psh
VertexLitGeneric_EnvMapV2.psh
vertexlitgeneric_flashlight_vs11.vsh
vertexlitgeneric_lightingonly_overbright2.psh
vertexlitgeneric_lightingonly_overbright2_ps11.fxc
VertexLitGeneric_MaskedEnvMapNoTexture.psh
VertexLitGeneric_MaskedEnvMapV2.psh
VertexLitGeneric_NoTexture.psh
VertexLitGeneric_SelfIlluminated.psh
VertexLitGeneric_SelfIlluminatedEnvMapV2.psh
VertexLitGeneric_SelfIlluminatedMaskedEnvMapV2.psh
VertexLitGeneric_SelfIllumOnly.vsh
VertexLitGeneric_vs11.vsh


vortwarp_ps2x.fxc
vortwarp_vs20.fxc

Water_ps2x.fxc
Water_vs20.fxc

WaterCheap_ps2x.fxc
WaterCheap_vs20.fxc

WorldVertexAlpha.psh
WorldVertexAlpha.vsh
worldvertexalpha_ps2x.fxc


WorldTwoTextureBlend.psh
WorldTwoTextureBlend_DetailAlpha.psh
WorldTwoTextureBlend_ps2x.fxc
WorldTwoTextureBlend_SelfIlluminated.psh

WorldVertexTransition.psh
WorldVertexTransition.vsh
WorldVertexTransition_ps14.psh
WorldVertexTransition_vs14.vsh
WorldVertexTransition_Seamless.psh
WorldVertexTransition_Seamless.vsh
WorldVertexTransition_BlendBase2.psh
WorldVertexTransition_Editor.psh


