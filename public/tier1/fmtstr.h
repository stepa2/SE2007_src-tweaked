//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: A simple class for performing safe and in-expression sprintf-style
//			string formatting
//
// $NoKeywords: $
//=============================================================================//

#ifndef FMTSTR_H
#define FMTSTR_H

#include <stdarg.h>
#include <stdio.h>
#include "tier0/platform.h"
#include "tier1/strtools.h"

#if defined( _WIN32 )
#pragma once
#endif

//=============================================================================

/*
// using macro to be compatable with GCC
#define FmtStrVSNPrintf( szBuf, nBufSize, pszFormat ) \
	do \
	{ \
		int     result; \
		va_list arg_ptr; \
	\
		va_start(arg_ptr, ((pszFormat))); \
		result = Q_vsnprintf((szBuf), (nBufSize)-1, ((pszFormat)), arg_ptr); \
		va_end(arg_ptr); \
	\
		(szBuf)[(nBufSize)-1] = 0; \
	} \
	while (0)
*/

inline void FmtStrVSNPrintf( char* szBuf, size_t nBufSize, const char* pszFormat, ... )
{
	va_list arg_ptr;
	va_start(arg_ptr, pszFormat);
	size_t result = Q_vsnprintf(szBuf, nBufSize-1, pszFormat, arg_ptr);
	va_end(arg_ptr);
	szBuf[nBufSize-1]='\0';
}

//-----------------------------------------------------------------------------
//
// Purpose: String formatter with specified size
//

template <int SIZE_BUF>
class CFmtStrN
{
public:
	CFmtStrN()									{ m_szBuf[0] = 0; }
	
	// Standard C formatting
	explicit CFmtStrN(const char *pszFormat, ...)
	{
		va_list arg_ptr;
		va_start(arg_ptr, pszFormat);
		size_t result = Q_vsnprintf(m_szBuf, SIZE_BUF-1, pszFormat, arg_ptr);
		va_end(arg_ptr);
		m_szBuf[SIZE_BUF-1]='\0';
	}

	// Use this for pass-through formatting
	CFmtStrN(const char ** pszFormat, va_list args)	{
		size_t result = Q_vsnprintf(m_szBuf, SIZE_BUF-1, args);
		m_szBuf[SIZE_BUF-1]='\0';
	}

	// Explicit reformat
	const char *format(const char *pszFormat, ...)	{
		va_list arg_ptr;
		va_start(arg_ptr, pszFormat);
		size_t result = Q_vsnprintf(m_szBuf, SIZE_BUF-1, pszFormat, arg_ptr);
		va_end(arg_ptr);
		m_szBuf[SIZE_BUF-1]='\0';
		return m_szBuf;
	}

	const char* format_va(const char* pszFormat, va_list args)
	{
		size_t result = Q_vsnprintf(m_szBuf, SIZE_BUF-1, args);
		m_szBuf[SIZE_BUF-1]='\0';
		return m_szBuf;
	}

	
	// Use this for pass-through formatting
	/*
	void VSprintf(const char **ppszFormat, ...)	{
		va_list arg_ptr;
		va_start(arg_ptr, *ppszFormat);
		size_t result = Q_vsnprintf(m_szBuf, SIZE_BUF-1, *ppszFormat, arg_ptr);
		va_end(arg_ptr);
		m_szBuf[SIZE_BUF-1]='\0'; }
	*/
	// Use for access
	operator const char *() const				{ return m_szBuf; }
	char *Access()								{ return m_szBuf; }

	void Clear()								{ m_szBuf[0] = 0; }

private:
	char m_szBuf[SIZE_BUF];
};

#undef FmtStrVSNPrintf

//-----------------------------------------------------------------------------
//
// Purpose: Default-sized string formatter
//

#define FMTSTR_STD_LEN 256

typedef CFmtStrN<FMTSTR_STD_LEN> CFmtStr;

//=============================================================================

#endif // FMTSTR_H
