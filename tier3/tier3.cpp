//===== Copyright � 2005-2005, Valve Corporation, All rights reserved. ======//
//
// Purpose: A higher level link library for general use in the game and tools.
//
//===========================================================================//

#include "tier3/tier3.h"
#include "tier0/dbg.h"
#include "istudiorender.h"
#include "vgui/IVGui.h"
#include "vgui/IInput.h"
#include "vgui/IPanel.h"
#include "vgui/ISurface.h"
#include "vgui/ILocalize.h"
#include "vgui/IScheme.h"
#include "vgui/ISystem.h"
#include "VGuiMatSurface/IMatSystemSurface.h"
#include "datacache/idatacache.h"
#include "datacache/imdlcache.h"
#include "avi/iavi.h"
#include "avi/ibik.h"
#include "movieobjects/idmemakefileutils.h"
#include "vphysics_interface.h"
#include "SoundEmitterSystem/isoundemittersystembase.h"


//-----------------------------------------------------------------------------
// These tier3 libraries must be set by any users of this library.
// They can be set by calling ConnectTier3Libraries.
// It is hoped that setting this, and using this library will be the common mechanism for
// allowing link libraries to access tier3 library interfaces
//-----------------------------------------------------------------------------
IStudioRender *g_pStudioRender = 0;
IStudioRender *studiorender = 0;
IMatSystemSurface *g_pMatSystemSurface = 0;
vgui::IInput *g_pVGuiInput = 0;
vgui::ISurface *g_pVGuiSurface = 0;
vgui::IPanel *g_pVGuiPanel = 0;
vgui::IVGui	*g_pVGui = 0;
vgui::ILocalize *g_pVGuiLocalize = 0;
vgui::ISchemeManager *g_pVGuiSchemeManager = 0;
vgui::ISystem *g_pVGuiSystem = 0;
IDataCache *g_pDataCache = 0;
IMDLCache *g_pMDLCache = 0;
IMDLCache *mdlcache = 0;
IAvi *g_pAVI = 0;
IBik *g_pBIK = 0;
IDmeMakefileUtils *g_pDmeMakefileUtils = 0;
IPhysicsCollision *g_pPhysicsCollision = 0;
ISoundEmitterSystemBase *g_pSoundEmitterSystem = 0;



void* FindSystem(CreateInterfaceFn *factoryList,int count, const char* name)
{
	for (size_t i = 0; i < count; i++)
	{
		void* result = factoryList[i](name, nullptr);
		if (result != nullptr) return result;
	}
	
	
	return nullptr;
}

//-----------------------------------------------------------------------------
// Call this to connect to all tier 3 libraries.
// It's up to the caller to check the globals it cares about to see if ones are missing
//-----------------------------------------------------------------------------
void ConnectTier3Libraries( CreateInterfaceFn *pFactoryList, int nFactoryCount )
{
	// Don't connect twice..
	Assert( !g_pStudioRender && !studiorender && !g_pMatSystemSurface && !g_pVGui && !g_pVGuiPanel && !g_pVGuiInput &&
		!g_pVGuiSurface && !g_pDataCache && !g_pMDLCache && !mdlcache && !g_pAVI && !g_pBIK && 
		!g_pDmeMakefileUtils && !g_pPhysicsCollision && !g_pVGuiLocalize && !g_pSoundEmitterSystem &&
		!g_pVGuiSchemeManager && !g_pVGuiSystem );

#define GEN_SYS_CONNECT(VARIABLE, INTERFACE) \
	VARIABLE = (decltype(VARIABLE))FindSystem(pFactoryList, nFactoryCount, INTERFACE) 

	GEN_SYS_CONNECT(g_pStudioRender, STUDIO_RENDER_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGui, VGUI_IVGUI_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGuiInput, VGUI_INPUT_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGuiPanel, VGUI_PANEL_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGuiSurface, VGUI_SURFACE_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGuiSchemeManager, VGUI_SCHEME_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGuiSystem, VGUI_SYSTEM_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pVGuiLocalize, VGUI_LOCALIZE_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pMatSystemSurface, MAT_SYSTEM_SURFACE_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pDataCache, DATACACHE_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pMDLCache, MDLCACHE_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pAVI, AVI_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pBIK, BIK_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pDmeMakefileUtils, DMEMAKEFILE_UTILS_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pPhysicsCollision, VPHYSICS_COLLISION_INTERFACE_VERSION);
	GEN_SYS_CONNECT(g_pSoundEmitterSystem, SOUNDEMITTERSYSTEM_INTERFACE_VERSION);
	
}

void DisconnectTier3Libraries()
{
	g_pStudioRender = 0;
	studiorender = 0;
	g_pVGui = 0;
	g_pVGuiInput = 0;
	g_pVGuiPanel = 0;
	g_pVGuiSurface = 0;
	g_pVGuiLocalize = 0;
	g_pVGuiSchemeManager = 0;
	g_pVGuiSystem = 0;
	g_pMatSystemSurface = 0;
	g_pDataCache = 0;
	g_pMDLCache = 0;
	mdlcache = 0;
	g_pAVI = 0;
	g_pBIK = 0;
	g_pPhysicsCollision = 0;
	g_pDmeMakefileUtils = NULL;
	g_pSoundEmitterSystem = 0;
}
