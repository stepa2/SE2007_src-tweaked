#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#endif	// _WIN32
#include "mathlib/mathlib.h"
#include "tier1/utlvector.h"
#include "vphysics_internal.h"

#ifdef _WIN32
HMODULE	gPhysicsDLLHandle;

#pragma warning (disable:4100)

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
 	if ( fdwReason == DLL_PROCESS_ATTACH )
    {
		MathLib_Init( 2.2f, 2.2f, 0.0f, 2.0f, false, false, false, false );
		// store out module handle
		gPhysicsDLLHandle = (HMODULE)hinstDLL;
	}
	else if ( fdwReason == DLL_PROCESS_DETACH )
    {
    }
	return TRUE;
}

#endif		// _WIN32


#include "tier1/interface.h"
#include "vphysics_interface.h"

extern IPhysicsEnvironment *CreatePhysicsEnvironment( void );
extern IPhysicsCollision *physcollision;

class CPhysicsInterface : public IPhysics
{
public:
	IPhysicsEnvironment *CreateEnvironment( void ) override
	{
		IPhysicsEnvironment *pEnvironment = CreatePhysicsEnvironment();
		m_envList.AddToTail( pEnvironment );
		return pEnvironment;
	}

	void DestroyEnvironment( IPhysicsEnvironment *pEnvironment ) override
	{
		m_envList.FindAndRemove( pEnvironment );
		delete pEnvironment;
	}

	IPhysicsEnvironment		*GetActiveEnvironmentByIndex( int index ) override
	{
		if ( index < 0 || index >= m_envList.Count() )
			return NULL;

		return m_envList[index];
	}

	bool Connect(CreateInterfaceFn factory) override {return true; }
	void Disconnect() override {}
	void* QueryInterface(const char* pInterfaceName) override
	{
		if ( !V_strcmp(VPHYSICS_COLLISION_INTERFACE_VERSION, pInterfaceName))
		{
			return physcollision;
		}
		return nullptr;
	}
	InitReturnVal_t Init() override { return INIT_OK; }
	void Shutdown() override {}
	
	IPhysicsObjectPairHash* CreateObjectPairHash() override
	{
		AssertMsg(false, "Unimplememnted");
		return nullptr;
	}
	void DestroyObjectPairHash(IPhysicsObjectPairHash* pHash) override
	{
		AssertMsg(false, "Unimplememnted");
	}
	IPhysicsCollisionSet* FindOrCreateCollisionSet(unsigned id, int maxElementCount) override
	{
		AssertMsg(false, "Unimplememnted");
		return nullptr;
	}
	IPhysicsCollisionSet* FindCollisionSet(unsigned id) override
	{
		AssertMsg(false, "Unimplememnted");
		return nullptr;
	}
	void DestroyAllCollisionSets() override
	{
		AssertMsg(false, "Unimplememnted");
	}
	
	CUtlVector< IPhysicsEnvironment *>	m_envList;
};


static CPhysicsInterface g_MainDLLInterface;
IPhysics *g_PhysicsInternal = &g_MainDLLInterface;
EXPOSE_SINGLE_INTERFACE_GLOBALVAR( CPhysicsInterface, IPhysics, VPHYSICS_INTERFACE_VERSION, g_MainDLLInterface );
