//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef PHYSICS_WORLD_H
#define PHYSICS_WORLD_H
#pragma once

#include "vphysics_interface.h"
#include "ivu_types.hxx"
#include "tier1/utlvector.h"

class IVP_Environment;
class CSleepObjects;
class CPhysicsListenerCollision;
class CPhysicsListenerConstraint;
class IVP_Listener_Collision;
class IVP_Listener_Constraint;
class IVP_Listener_Object;
class IVP_Controller;
class CPhysicsFluidController;
class CCollisionSolver;
class CPhysicsObject;
class CDeleteQueue;

class CPhysicsEnvironment : public IPhysicsEnvironment
{
public:
	CPhysicsEnvironment( void );
	~CPhysicsEnvironment( void );
	void			SetGravity( const Vector& gravityVector ) override;
	IPhysicsObject	*CreatePolyObject( const CPhysCollide *pCollisionModel, int materialIndex, const Vector& position, const QAngle& angles, objectparams_t *pParams ) override;
	IPhysicsObject	*CreatePolyObjectStatic( const CPhysCollide *pCollisionModel, int materialIndex, const Vector& position, const QAngle& angles, objectparams_t *pParams ) override;
	IPhysicsSpring	*CreateSpring( IPhysicsObject *pObjectStart, IPhysicsObject *pObjectEnd, springparams_t *pParams ) override;
	IPhysicsFluidController	*CreateFluidController( IPhysicsObject *pFluidObject, fluidparams_t *pParams ) override;
	IPhysicsConstraint *CreateRagdollConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_ragdollparams_t &ragdoll ) override;

	IPhysicsConstraint *CreateHingeConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_hingeparams_t &hinge ) override;
	IPhysicsConstraint *CreateFixedConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_fixedparams_t &fixed ) override;
	IPhysicsConstraint *CreateSlidingConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_slidingparams_t &sliding ) override;
	IPhysicsConstraint *CreateBallsocketConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_ballsocketparams_t &ballsocket ) override;
	IPhysicsConstraint *CreatePulleyConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_pulleyparams_t &pulley ) override;
	IPhysicsConstraint *CreateLengthConstraint( IPhysicsObject *pReferenceObject, IPhysicsObject *pAttachedObject, IPhysicsConstraintGroup *pGroup, const constraint_lengthparams_t &length ) override;

	virtual IPhysicsConstraintGroup *CreateConstraintGroup( void );
	void DestroyConstraintGroup( IPhysicsConstraintGroup *pGroup ) override;

	virtual void	EnableCollisions( IPhysicsObject *pObject1, IPhysicsObject *pObject2 );
	virtual void	DisableCollisions( IPhysicsObject *pObject1, IPhysicsObject *pObject2 );
	virtual bool	ShouldCollide( IPhysicsObject *pObject1, IPhysicsObject *pObject2 );

	void			Simulate( float deltaTime ) override;
	float			GetSimulationTimestep( void ) const override;
	void			SetSimulationTimestep( float timestep ) override;
	float			GetSimulationTime( void ) const override;
	int				GetTimestepsSimulatedLast() const;
	bool			IsInSimulation( void ) const override;

	void DestroyObject( IPhysicsObject * ) override;
	void DestroySpring( IPhysicsSpring * ) override;
	void DestroyFluidController( IPhysicsFluidController * ) override;
	void DestroyConstraint( IPhysicsConstraint * ) override;

	void SetCollisionEventHandler( IPhysicsCollisionEvent *pCollisionEvents ) override;
	void SetObjectEventHandler( IPhysicsObjectEvent *pObjectEvents ) override;
	void SetConstraintEventHandler( IPhysicsConstraintEvent *pConstraintEvents ) override;

	IPhysicsShadowController *CreateShadowController( IPhysicsObject *pObject, bool allowTranslation, bool allowRotation ) override;
	void DestroyShadowController( IPhysicsShadowController * ) override;
	IPhysicsMotionController *CreateMotionController( IMotionEvent *pHandler ) override;
	void DestroyMotionController( IPhysicsMotionController *pController ) override;
	IPhysicsPlayerController *CreatePlayerController( IPhysicsObject *pObject ) override;
	void DestroyPlayerController( IPhysicsPlayerController *pController ) override;
	IPhysicsVehicleController *CreateVehicleController( IPhysicsObject *pVehicleBodyObject, const vehicleparams_t &params, unsigned int nVehicleType, IPhysicsGameTrace *pGameTrace ) override;
	void DestroyVehicleController( IPhysicsVehicleController *pController ) override;

	void SetQuickDelete( bool bQuick ) override
	{
		m_deleteQuick = true;
	}
	virtual bool ShouldQuickDelete() { return m_deleteQuick; }
	virtual void TraceBox( trace_t *ptr, const Vector &mins, const Vector &maxs, const Vector &start, const Vector &end );
	void SetCollisionSolver( IPhysicsCollisionSolver *pCollisionSolver ) override;
	void GetGravity( Vector* gravityVector ) const override;
	int	 GetActiveObjectCount() const override;
	void GetActiveObjects( IPhysicsObject **pOutputObjectList ) const override;

	IVP_Environment	*GetIVPEnvironment( void ) const { return m_pPhysEnv; }
	void		ClearDeadObjects( void );
	IVP_Controller *GetDragController() const { return m_pDragController; }
	void SetAirDensity( float density ) override;
	float GetAirDensity() const override;
	void ResetSimulationClock( void ) override;
	IPhysicsObject *CreateSphereObject( float radius, int materialIndex, const Vector& position, const QAngle& angles, objectparams_t *pParams, bool isStatic ) override;
	void CleanupDeleteList() override;
	void EnableDeleteQueue( bool enable ) override { m_queueDeleteObject = enable; }
	// debug
	bool IsCollisionModelUsed( CPhysCollide *pCollide ) const override;

	// scale per-psi sim quantities
	// This is 1 / number of PSIs during this call to Simulate
	inline float GetInvPSIScale( void ) { return m_invPSIscale; }
	inline int GetSimulatedPSIs( void ) { return m_simPSIs; }
	void EventPSI( void );

	// Save/restore
	bool Save( const physsaveparams_t &params  ) override;
	void PreRestore( const physprerestoreparams_t &params ) override;
	bool Restore( const physrestoreparams_t &params ) override;
	void PostRestore() override;
	void PhantomAdd( CPhysicsObject *pObject );
	void PhantomRemove( CPhysicsObject *pObject );

	void SetDebugOverlay(CreateInterfaceFn debugOverlayFactory) override
	{
		AssertMsg(false, "Unimplemented");
	}
	IVPhysicsDebugOverlay* GetDebugOverlay() override
	{
		AssertMsg(false, "Unimplemented");
		return nullptr;
	}
	IPhysicsConstraintGroup* CreateConstraintGroup(const constraint_groupparams_t& groupParams) override
	{
		AssertMsg(false, "Unimplemented");
		return nullptr;
	}
	float GetNextFrameTime() const override
	{
		AssertMsg(false, "Unimplemented");
		return 0;
	}
	const IPhysicsObject** GetObjectList(int* pOutputObjectCount) const override
	{
		AssertMsg(false, "Unimplemented");
		return nullptr;
	}
	bool TransferObject(IPhysicsObject* pObject, IPhysicsEnvironment* pDestinationEnvironment) override
	{
		AssertMsg(false, "Unimplemented");
		return false;
	}
	void TraceRay(const Ray_t& ray, unsigned fMask, IPhysicsTraceFilter* pTraceFilter, trace_t* pTrace) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void SweepCollideable(const CPhysCollide* pCollide, const Vector& vecAbsStart, const Vector& vecAbsEnd,
		const QAngle& vecAngles, unsigned fMask, IPhysicsTraceFilter* pTraceFilter, trace_t* pTrace) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void GetPerformanceSettings(physics_performanceparams_t* pOutput) const override
	{
		AssertMsg(false, "Unimplemented");
	}
	void SetPerformanceSettings(const physics_performanceparams_t* pSettings) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void ReadStats(physics_stats_t* pOutput) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void ClearStats() override
	{
		AssertMsg(false, "Unimplemented");
	}
	unsigned GetObjectSerializeSize(IPhysicsObject* pObject) const override
	{
		AssertMsg(false, "Unimplemented");
		return 0;
	}
	void SerializeObjectToBuffer(IPhysicsObject* pObject, unsigned char* pBuffer, unsigned bufferSize) override
	{
		AssertMsg(false, "Unimplemented");
	}
	IPhysicsObject* UnserializeObjectFromBuffer(void* pGameData, unsigned char* pBuffer, unsigned bufferSize,
		bool enableCollisions) override
	{
		AssertMsg(false, "Unimplemented");
		return nullptr;
	}
	void EnableConstraintNotify(bool bEnable) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void DebugCheckContacts() override
	{
		AssertMsg(false, "Unimplemented");
	}
private:
	bool							m_inSimulation;
	bool							m_queueDeleteObject;
	IVP_Environment					*m_pPhysEnv;
	IVP_Controller					*m_pDragController;
	CUtlVector<IPhysicsObject *>	m_objects;
	CUtlVector<IPhysicsObject *>	m_deadObjects;
	CUtlVector<CPhysicsFluidController *> m_fluids;
	CSleepObjects					*m_pSleepEvents;
	CPhysicsListenerCollision		*m_pCollisionListener;
	CCollisionSolver				*m_pCollisionSolver;
	CPhysicsListenerConstraint		*m_pConstraintListener;
	CDeleteQueue					*m_pDeleteQueue;
	bool							m_deleteQuick;
	int								m_simPSIs;		// number of PSIs this call to simulate
	int								m_simPSIcurrent;
	float							m_invPSIscale;  // inverse of simPSIs
};

extern IPhysicsEnvironment *CreatePhysicsEnvironment( void );

class IVP_Synapse_Friction;
class IVP_Real_Object;
extern IVP_Real_Object *GetOppositeSynapseObject( IVP_Synapse_Friction *pfriction );

#endif // PHYSICS_WORLD_H
