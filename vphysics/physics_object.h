//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef PHYSICS_OBJECT_H
#define PHYSICS_OBJECT_H

#ifdef _WIN32
#pragma once
#endif

#include "vphysics_interface.h"

class IVP_Real_Object;
class IVP_Environment;
class IVP_U_Float_Point;
class IVP_SurfaceManager;
class IVP_Controller;
class CPhysicsEnvironment;
struct vphysics_save_cphysicsobject_t;

enum
{
	OBJ_AWAKE = 0,			// awake, simulating
	OBJ_STARTSLEEP = 1,		// going to sleep, but not queried yet
	OBJ_SLEEP = 2,			// sleeping, no state changes since last query
};


class CPhysicsObject : public IPhysicsObject
{
public:
	CPhysicsObject( void );
	virtual ~CPhysicsObject( void );

	void			Init( IVP_Real_Object *pObject, int materialIndex, float volume, float drag, float angDrag, const Vector *massCenterOverride );

	// IPhysicsObject functions
	bool			IsStatic() const override;
	bool			IsMoveable() const override;
	void			Wake() override;
	void			Sleep( void ) override;
	bool			IsAsleep( void ) const override;
	void			SetGameData( void *pAppData ) override;
	void			*GetGameData( void ) const override;
	void			SetCallbackFlags( unsigned short callbackflags ) override;
	unsigned short	GetCallbackFlags( void ) const override;
	void			SetGameFlags( unsigned short userFlags ) override;
	unsigned short	GetGameFlags( void ) const override;
	void			SetMass( float mass ) override;
	float			GetMass( void ) const override;
	float			GetInvMass( void ) const override;
	void			EnableCollisions( bool enable ) override;
	// Enable / disable gravity for this object
	void			EnableGravity( bool enable ) override;
	// Enable / disable air friction / drag for this object
	void			EnableDrag( bool enable ) override;
	void			EnableMotion( bool enable ) override;

	bool			IsCollisionEnabled() const override;
	bool			IsGravityEnabled() const override;
	bool			IsDragEnabled() const override;
	bool			IsMotionEnabled() const override;

	void			LocalToWorld( Vector *worldPosition, const Vector &localPosition ) const override;
	void			WorldToLocal( Vector *localPosition, const Vector &worldPosition ) const override;
	void			LocalToWorldVector( Vector *worldVector, const Vector &localVector ) const override;
	void			WorldToLocalVector( Vector *localVector, const Vector &worldVector ) const override;
	void			ApplyForceCenter( const Vector &forceVector ) override;
	void			ApplyForceOffset( const Vector &forceVector, const Vector &worldPosition ) override;
	void			ApplyTorqueCenter( const AngularImpulse & ) override;
	void			CalculateForceOffset( const Vector &forceVector, const Vector &worldPosition, Vector *centerForce, AngularImpulse *centerTorque ) const override;
	void			CalculateVelocityOffset( const Vector &forceVector, const Vector &worldPosition, Vector *centerVelocity, AngularImpulse *centerAngularVelocity ) const override;
	void			GetPosition( Vector *worldPosition, QAngle *angles ) const override;
	void			GetPositionMatrix( matrix3x4_t* positionMatrix ) const override;
	void			SetPosition( const Vector &worldPosition, const QAngle &angles, bool isTeleport = false ) override;
	void			SetPositionMatrix( const matrix3x4_t& matrix, bool isTeleport = false  ) override;
	void			SetVelocity( const Vector *velocity, const AngularImpulse *angularVelocity ) override;
	void			GetVelocity( Vector *velocity, AngularImpulse *angularVelocity ) const override;
	void			AddVelocity( const Vector *velocity, const AngularImpulse *angularVelocity ) override;
	bool			GetContactPoint( Vector *contactPoint, IPhysicsObject **contactObject ) const override;
	void			SetShadow( float maxVelocity, float maxAngularVelocity, bool allowPhysicsMovement, bool allowPhysicsRotation ) override;
	void			UpdateShadow( const Vector &targetPosition, const QAngle &targetAngles, bool tempDisableGravity, float timeOffset ) override;
	void			RemoveShadowController() override;
	int				GetShadowPosition( Vector *position, QAngle *angles ) const override;
	
	IPhysicsShadowController *GetShadowController( void ) const override;
	Vector			GetInertia( void ) const override;
	Vector			GetInvInertia( void ) const override;
	void			GetVelocityAtPoint( const Vector &worldPosition, Vector *velocity ) const override;
	void			SetInertia( const Vector &inertia ) override;
	void			GetDamping( float *speed, float *rot ) const override;
	void			SetDamping( const float *speed, const float *rot ) override;
	const CPhysCollide	*GetCollide( void ) const override;
	float			CalculateLinearDrag( const Vector &unitDirection ) const override;
	float			CalculateAngularDrag( const Vector &objectSpaceRotationAxis ) const override;

	float			GetDragInDirection( const IVP_U_Float_Point &dir ) const;
	float			GetAngularDragInDirection( const IVP_U_Float_Point &angVelocity ) const;
	char const		*GetName() const override;
	int				GetMaterialIndex() const override { return GetMaterialIndexInternal(); }
	void			SetMaterialIndex( int materialIndex ) override;
	float			GetEnergy() const override;
	void			RecheckCollisionFilter() override;
	float			ComputeShadowControl( const hlshadowcontrol_params_t &params, float secondsToArrival, float dt ) override;
	float			GetRollingDrag() const { return m_rollingDrag; }
	void			SetRollingDrag( float drag ) { m_rollingDrag = drag; }
	void			SetDragCoefficient( float *pDrag, float *pAngularDrag ) override;
	float			GetSphereRadius() const override;
	void			BecomeTrigger() override;
	void			RemoveTrigger() override;
	bool			IsTrigger() const override;
	bool			IsFluid() const override;
	void			SetBuoyancyRatio( float ratio ) override;
	unsigned int	GetContents() const override { return m_contentsMask; }
	void			SetContents( unsigned int contents ) override;

	// local functions
	inline	IVP_Real_Object *GetObject( void ) const { return m_pObject; }
	inline int		CallbackFlags( void ) const { return m_callbacks; }
	void			NotifySleep( void );
	void			NotifyWake( void );
	int				GetSleepState( void ) const { return m_sleepState; }

	inline int		GetMaterialIndexInternal( void ) const { return m_materialIndex; }
	inline int		GetActiveIndex( void ) const { return m_activeIndex; }
	inline void		SetActiveIndex( int index ) { m_activeIndex = index; }
	inline float	GetBuoyancyRatio( void ) const { return m_buoyancyRatio; }

	IVP_SurfaceManager *GetSurfaceManager( void ) const;

	void			WriteToTemplate( vphysics_save_cphysicsobject_t &objectTemplate ) const;
	void			InitFromTemplate( CPhysicsEnvironment *pEnvironment, void *pGameData, const vphysics_save_cphysicsobject_t &objectTemplate );

	public:
	bool IsHinged() const override
	{
		AssertMsg(false, "Unimplemented");
		return false;
	}
	bool IsAttachedToConstraint(bool bExternalOnly) const override
	{
		AssertMsg(false, "Unimplemented");
		return false;
	}
	void SetGameIndex(unsigned short gameIndex) override
	{
		AssertMsg(false, "Unimplemented");
	}
	unsigned short GetGameIndex() const override
	{
		AssertMsg(false, "Unimplemented");
		return 0;
	}
	void RecheckContactPoints() override
	{
		AssertMsg(false, "Unimplemented");
	}
	Vector GetMassCenterLocalSpace() const override
	{
		AssertMsg(false, "Unimplemented");
		return Vector(0,0,0);
	}
	void SetVelocityInstantaneous(const Vector* velocity, const AngularImpulse* angularVelocity) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void GetImplicitVelocity(Vector* velocity, AngularImpulse* angularVelocity) const override
	{
		AssertMsg(false, "Unimplemented");
	}
	void BecomeHinged(int localAxis) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void RemoveHinged() override
	{
		AssertMsg(false, "Unimplemented");
	}
	IPhysicsFrictionSnapshot* CreateFrictionSnapshot() override
	{
		AssertMsg(false, "Unimplemented");
		return nullptr;
	}
	void DestroyFrictionSnapshot(IPhysicsFrictionSnapshot* pSnapshot) override
	{
		AssertMsg(false, "Unimplemented");
	}
	void OutputDebugInfo() const override
	{
		AssertMsg(false, "Unimplemented");
	}
	
private:
	// NOTE: Local to vphysics, used to save/restore shadow controller
	void			RestoreShadowController( IPhysicsShadowController *pShadowController );
	friend bool RestorePhysicsObject( const physrestoreparams_t &params, CPhysicsObject **ppObject );

	CPhysicsEnvironment	*GetVPhysicsEnvironment() const;
	bool				IsControlling( IVP_Controller *pController ) const;
	float				GetVolume() const;
	void				SetVolume( float volume );


private:
	void			*m_pGameData;
	IVP_Real_Object	*m_pObject;
	short			m_sleepState;
	// HACKHACK: Public for now
public:
	short			m_collideType;
private:
	unsigned short	m_materialIndex;
	unsigned short	m_activeIndex;

	unsigned short	m_callbacks;
	unsigned short	m_gameFlags;
	unsigned int	m_contentsMask;
	
	float			m_volume;
	float			m_buoyancyRatio;
	float			m_dragCoefficient;
	float			m_angDragCoefficient;
	float			m_rollingDrag;
	Vector			m_dragBasis;
	Vector			m_angDragBasis;
	Vector			m_massCenterOverride;
	IPhysicsShadowController	*m_pShadow;
	bool			m_shadowTempGravityDisable;
};

extern CPhysicsObject *CreatePhysicsObject( CPhysicsEnvironment *pEnvironment, const CPhysCollide *pCollisionModel, int materialIndex, const Vector &position, const QAngle &angles, objectparams_t *pParams, bool isStatic );
extern CPhysicsObject *CreatePhysicsSphere( CPhysicsEnvironment *pEnvironment, float radius, int materialIndex, const Vector &position, const QAngle &angles, objectparams_t *pParams, bool isStatic );

#endif // PHYSICS_OBJECT_H
